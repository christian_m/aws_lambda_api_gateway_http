output "api_url" {
  value = aws_apigatewayv2_stage.default.invoke_url
}

output "access_log_group_name" {
  value = aws_cloudwatch_log_group.api_access_log.name
}