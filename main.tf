resource "aws_apigatewayv2_api" "default" {
  name          = local.api_name
  protocol_type = "HTTP"
  description   = "${var.lambda_name} Lambda Function API"

  cors_configuration {
    allow_credentials = false
    allow_headers     = ["*"]
    allow_methods     = ["GET"]
    allow_origins     = ["*"]
    expose_headers    = ["*"]
    max_age           = 3600
  }

  tags = {
    env = var.environment
  }
}

resource "aws_apigatewayv2_route" "default" {
  api_id    = aws_apigatewayv2_api.default.id
  route_key = "GET /{proxy+}"
  target    = "integrations/${aws_apigatewayv2_integration.default.id}"
}

resource "aws_apigatewayv2_integration" "default" {
  api_id           = aws_apigatewayv2_api.default.id
  integration_type = "AWS_PROXY"

  connection_type      = "INTERNET"
  description          = "${local.api_name} {proxy+} integration"
  integration_method   = "POST"
  integration_uri      = var.lambda_function_invoke_arn
  passthrough_behavior = "WHEN_NO_MATCH"

  lifecycle {
    ignore_changes = [
      passthrough_behavior
    ]
  }
}

resource "aws_apigatewayv2_stage" "default" {
  api_id      = aws_apigatewayv2_api.default.id
  name        = "$default"
  auto_deploy = true
  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_access_log.arn
    format          = jsonencode(
      {
        httpMethod     = "$context.httpMethod"
        ip             = "$context.identity.sourceIp"
        protocol       = "$context.protocol"
        requestId      = "$context.requestId"
        requestTime    = "$context.requestTime"
        responseLength = "$context.responseLength"
        routeKey       = "$context.routeKey"
        status         = "$context.status"
      }
    )
  }

  lifecycle {
    ignore_changes = [
      deployment_id,
      default_route_settings
    ]
  }

  tags = {
    env = var.environment
  }
}

resource "aws_lambda_permission" "allow_api_gateway_invoke" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.default.execution_arn}/*/*/{proxy+}"
}

module "domain_certificate" {
  source      = "git::git@bitbucket.org:christian_m/aws_domain_certificate.git?ref=v1.0"
  environment = var.environment
  domain_name = var.domain_name
  zone_id     = var.zone_id
}

resource "aws_apigatewayv2_domain_name" "default" {
  domain_name = var.domain_name

  domain_name_configuration {
    certificate_arn = module.domain_certificate.arn
    endpoint_type   = "REGIONAL"
    security_policy = "TLS_1_2"
  }

  tags = {
    env = var.environment
  }
}

module "gateway_domain_alias" {
  source          = "git::git@bitbucket.org:christian_m/aws_route53_resource_alias.git?ref=v1.0"
  environment     = var.environment
  allow_overwrite = true
  zone_id         = var.zone_id
  record          = {
    name = var.domain_name
    type = "A"
  }
  aliases = [
    {
      evaluate_target_health = false
      name                   = aws_apigatewayv2_domain_name.default.domain_name_configuration[0].target_domain_name
      zone_id                = aws_apigatewayv2_domain_name.default.domain_name_configuration[0].hosted_zone_id
    },
  ]
}

resource "aws_apigatewayv2_api_mapping" "example" {
  api_id      = aws_apigatewayv2_api.default.id
  domain_name = aws_apigatewayv2_domain_name.default.id
  stage       = aws_apigatewayv2_stage.default.id
}